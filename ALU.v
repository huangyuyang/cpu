`timescale 1ns / 10ps

module ALU (
		input stall,
		input [31:0] input1,
		input [31:0] input2,
		input [3:0] aluOperation,

		output reg [31:0] aluResult,
		output reg zero
	);

	always @ (input1 or input2 or aluOperation)
	begin
		if (stall == 0)
		begin
			case (aluOperation)
				4'b0000 : aluResult = input1 & input2;
				4'b0001 : aluResult = input1 | input2;
				4'b0010 : aluResult = input1 + input2;
				4'b0110 : aluResult = input1 - input2;
				4'b0111 : aluResult = input1 < input2;
				4'b1100 : aluResult = ~(input1 | input2);
				4'b1110 : aluResult = input1 * input2;
			endcase

			zero = (aluResult == 0) ? 1 : 0;
		end
	end
endmodule