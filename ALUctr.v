`timescale 1ns / 10ps

module ALUctr (
		input stall,
		input [1:0] aluOp,
		input [5:0] func,

		output reg [3:0] aluOperation
	);

	initial
	begin
		aluOperation = 4'b0000;
	end

	always @ (aluOp or func)
	begin
		if (stall == 0)
		begin
			casex ({aluOp, func})
				8'b00xxxxxx : aluOperation = 4'b0010;
				8'b01xxxxxx : aluOperation = 4'b0110;
				8'b1xxx0000 : aluOperation = 4'b0010;
				8'b1xxx0010 : aluOperation = 4'b0110;
				8'b1xxx0100 : aluOperation = 4'b0000;
				8'b1xxx0101 : aluOperation = 4'b0001;
				8'b1xxx1010 : aluOperation = 4'b0111;
				8'b1xxx1110 : aluOperation = 4'b1110;
				default: aluOperation = 4'b1111;
			endcase
		end
	end
endmodule
