`timescale 1ns / 10ps

module MainCtr (
		input stall,
		input [5:0] opCode,
		input noOp,

		output reg notLockMem,
		output reg regDst,
		output reg aluSrc,
		output reg memToReg,
		output reg regWrite,
		output reg memRead,
		output reg memWrite,
		output reg branch,
		output reg [1:0] aluOperation,
		output reg jump
	);
	
	always @ (opCode or noOp)
	begin
		if (stall == 0)
		begin
			case (opCode)
				6'b001000:
				begin
					notLockMem = 0;
					regDst = 0;
					aluSrc = 1;
					memToReg = 0;
					regWrite = 1;
					memRead = 0;
					memWrite = 0;
					branch = 0;
					aluOperation = 2'b00;
					jump = 0;
				end
				6'b000010:
				begin
					notLockMem = 0;
					regDst = 0;
					aluSrc = 0;
					memToReg = 0;
					regWrite = 0;
					memRead = 0;
					memWrite = 0;
					branch = 0;
					aluOperation = 2'b00;
					jump = 1;
				end
				6'b000000:
				begin
					notLockMem = 0;
					regDst = 1;
					aluSrc = 0;
					memToReg = 0;
					regWrite = 1;
					memRead = 0;
					memWrite = 0;
					branch = 0;
					aluOperation = 2'b10;
					jump = 0;
				end
				6'b100011:
				begin
					notLockMem = 1;
					regDst = 0;
					aluSrc = 1;
					memToReg = 1;
					regWrite = 1;
					memRead = 1;
					memWrite = 0;
					branch = 0;
					aluOperation = 2'b00;
					jump = 0;
				end
				6'b101011:
				begin
					notLockMem = 1;
					regDst = 0;
					aluSrc = 1;
					memToReg = 0;
					regWrite = 0;
					memRead = 0;
					memWrite = 1;
					branch = 0;
					aluOperation = 2'b0;
					jump = 0;
				end
				6'b000100:
				begin
					notLockMem = 0;
					regDst = 0;
					aluSrc = 0;
					memToReg = 0;
					regWrite = 0;
					memRead = 0;
					memWrite = 0;
					branch = 1;
					aluOperation = 2'b01;
					jump = 0;
				end
				default:
				begin
					notLockMem = 0;
					regDst = 0;
					aluSrc = 0;
					memToReg = 0;
					regWrite = 0;
					memRead = 0;
					memWrite = 0;
					branch = 0;
					aluOperation = 2'b0;
					jump = 0;
				end
			endcase
			if (noOp)
			begin
				notLockMem = 0;
				regDst = 0;
				aluSrc = 0;
				memToReg = 0;
				regWrite = 0;
				memRead = 0;
				memWrite = 0;
				branch = 0;
				aluOperation = 2'b0;
				jump = 0;
			end
		end
	end
endmodule
