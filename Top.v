`timescale 1ns / 10ps

module Top (
		input mainClock,
		input reset,

		output wire [31:0] PC_ADDRESS
	);

	wire [31:0] RAW_INST; 

	//decode stage
	wire REG_DST, JUMP, BRANCH, MEM_READ, MEM_TO_REG, NOT_LOCK_MEM, MEM_WRITE, ALU_SRC, REG_WRITE;
	wire [1:0] ALU_OP;

	//IF stage
	wire [31:0] INST, PC_PLUS_4;

	//ID stage
	wire [31:0] INST2, PC_PLUS_4_2, READ_DATA1, READ_DATA2, SIGN_EXT_DATA;

	//EX stage
	wire REG_DST2, BRANCH2, MEM_READ2, MEM_TO_REG2, NOT_LOCK_MEM2, MEM_WRITE2, ALU_SRC2,REG_WRITE2, ZERO;
	wire [1:0] ALU_OP2;
	wire [4:0] WRITE_REG, RT, RD, RS;
	wire [31:0] PC_PLUS_4_3, BRANCH_ADDR, READ_DATA1_2, READ_DATA2_2, SIGN_EXT_DATA2, ALU_RESULT;

	//MEM stage
	wire BRANCH3, MEM_READ3, MEM_TO_REG3, NOT_LOCK_MEM3, MEM_WRITE3, REG_WRITE3, ZERO2, MEM_READ_SUCCESS;
	wire [4:0] WRITE_REG2;
	wire [31:0] BRANCH_ADDR2, READ_DATA2_3, ALU_RESULT2, MEM_READ_DATA;

	//WB stage
	wire MEM_TO_REG4, REG_WRITE4;
	wire [4:0] WRITE_REG3;
	wire [31:0] ALU_RESULT3, MEM_READ_DATA2;

	//template, jump, branch
	wire [31:0] TMP1, TMP2;
	wire BRANCH_AND_ZERO;
	wire [31:0] PC_SOURCE, PC_OR_BRANCH_ADDR, JUMP_ADDR, PC_P4_P_BRANCH_ADDR;
	wire [31:0] ALU_INPUT2;
	wire [3:0] ALU_CTR;
	wire [31:0] WRITE_REG_DATA;
	wire PC_WRITE, IFID_WRITE, NOOP;
	wire FLUSH_BY_BRANCH;
	wire IF_FLUSH;

	//hazard
	wire [1:0] FORWARD_A, FORWARD_B;
	wire [31:0] ALU_SOURCE1, ALU_SOURCE2;

	assign PC_PLUS_4 = PC_ADDRESS + 4;
	assign TMP1 = INST2[25:0] << 2;
	assign JUMP_ADDR = (PC_PLUS_4_2 & 32'b11110000000000000000000000000000) + TMP1;
	assign TMP2 = SIGN_EXT_DATA2 << 2;
	assign BRANCH_ADDR = TMP2 + PC_PLUS_4_3;
	assign STALL = ~MEM_READ_SUCCESS;

	MainCtr mainCtr (
		.stall(STALL),
		.opCode(INST2[31:26]),
		.notLockMem(NOT_LOCK_MEM),
		.regDst(REG_DST),
		.jump(JUMP),
		.branch(BRANCH),
		.memRead(MEM_READ),
		.memToReg(MEM_TO_REG),
		.aluOperation(ALU_OP),
		.memWrite(MEM_WRITE),
		.aluSrc(ALU_SRC),
		.regWrite(REG_WRITE),
		.noOp(NOOP)
	);

	PC pc (
		.stall(STALL),
		.clockIn(mainClock),
		.reset(reset),
		.source(PC_SOURCE),
		.pc(PC_ADDRESS),
		.pcWrite(PC_WRITE)
	);

	InstMemory instMem (
		.stall(STALL),
		.address(PC_ADDRESS),
		.readData(RAW_INST)
	);

	assign WRITE_REG = REG_DST2 ? RD : RT;

	Register regi (
		.clockIn(mainClock),
		.reset(reset),
		.regWrite(REG_WRITE4),
		.readReg1(INST2[25:21]),
		.readReg2(INST2[20:16]),
		.writeReg(WRITE_REG3),
		.writeData(WRITE_REG_DATA),
		.readData1(READ_DATA1),
		.readData2(READ_DATA2)
	);
	
	Signext signExt (
		.inst(INST2[15:0]),
		.data(SIGN_EXT_DATA)
	);
	
	assign ALU_INPUT2 = ALU_SRC2 ? SIGN_EXT_DATA2 : ALU_SOURCE2;
	
	ALUctr aluCtr (
		.stall(STALL),
		.aluOp(ALU_OP2),
		.func(SIGN_EXT_DATA2[5:0]),
		.aluOperation(ALU_CTR)
	);	
	
	ALU alu (
		.stall(STALL),
		.input1(ALU_SOURCE1),
		.input2(ALU_INPUT2),
		.aluOperation(ALU_CTR),
		.zero(ZERO),
		.aluResult(ALU_RESULT)
	);
		
	DataMemory dataMemory (
		.stall(STALL),
		.clockIn(mainClock),
		.notLock(NOT_LOCK_MEM3),
		.memWrite(MEM_WRITE3),
		.memRead(MEM_READ3),
		.address(ALU_RESULT2),
		.writeData(READ_DATA2_3),
		.readData(MEM_READ_DATA),
		.success(MEM_READ_SUCCESS)
	);
	
	assign WRITE_REG_DATA = MEM_TO_REG4 ? MEM_READ_DATA2 : ALU_RESULT3;
	assign PC_OR_BRANCH_ADDR = BRANCH_AND_ZERO ? PC_P4_P_BRANCH_ADDR : PC_PLUS_4;
	assign PC_SOURCE = JUMP ? JUMP_ADDR : PC_OR_BRANCH_ADDR;
	
	mux_3 readData1Out (
		.stall(STALL),
		.input0(READ_DATA1_2),
		.input1(WRITE_REG_DATA),
		.input2(ALU_RESULT2),
		.forward(FORWARD_A),
		.out(ALU_SOURCE1)
	);
	
	mux_3 readData2Out (
		.stall(STALL),
		.input0(READ_DATA2_2),
		.input1(WRITE_REG_DATA),
		.input2(ALU_RESULT2),
		.forward(FORWARD_B),
		.out(ALU_SOURCE2)
	);
	
	ForwardUnit forwardUnit (
		.stall(STALL),
		.rt_idex(RT),
		.rs_idex(RS),
		.rd_exmem(WRITE_REG2),
		.rd_memwb(WRITE_REG3),
		.reg_write_exmem(REG_WRITE3),
		.reg_write_memwb(REG_WRITE4),
		.forwardA(FORWARD_A),
		.forwardB(FORWARD_B)
	);
	
	IF_ID IFID (
		.stall(STALL),
		.clk(mainClock),
		.pc(PC_PLUS_4),
		.inst(INST),
		.ifid_write(IFID_WRITE),
		.pc_out(PC_PLUS_4_2),
		.inst_out(INST2)
	);
	
	ID_EX IDEX (
		.stall(STALL),
		.clk(mainClock),
		.regDst(REG_DST),
		.aluSrc(ALU_SRC),
		.memToReg(MEM_TO_REG),
		.regWrite(REG_WRITE),
		.memRead(MEM_READ),
		.notLockMem(NOT_LOCK_MEM),
		.memWrite(MEM_WRITE),
		.branch(BRANCH),
		.aluOp(ALU_OP),

		.regDst_out(REG_DST2),
		.aluSrc_out(ALU_SRC2),
		.memToReg_out(MEM_TO_REG2),
		.regWrite_out(REG_WRITE2),
		.memRead_out(MEM_READ2),
		.notLockMem_out(NOT_LOCK_MEM2),
		.memWrite_out(MEM_WRITE2),
		.branch_out(BRANCH2),
		.aluOp_out(ALU_OP2),

		.pc(PC_PLUS_4_2),
		.immediate(SIGN_EXT_DATA),
		.readData1(READ_DATA1),
		.readData2(READ_DATA2),
		.rd(INST2[15:11]),
		.rt(INST2[20:16]),
		.rs(INST2[25:21]),

		.pc_out(PC_PLUS_4_3),
		.immediate_out(SIGN_EXT_DATA2),
		.readData1_out(READ_DATA1_2),
		.readData2_out(READ_DATA2_2),
		.rt_out(RT),
		.rd_out(RD),
		.rs_out(RS)
	);
	
	EX_MEM EXMEM (
		.stall(STALL),
		.clk(mainClock), 
		.memToReg(MEM_TO_REG2),
		.regWrite(REG_WRITE2),
		.memRead(MEM_READ2),
		.notLockMem(NOT_LOCK_MEM2),
		.memWrite(MEM_WRITE2),
		.branch(BRANCH2),
		.branchAddr(BRANCH_ADDR),
		.aluResult(ALU_RESULT),
		.readData2(ALU_SOURCE2),
		.rt_or_rd(WRITE_REG),
		.zero(ZERO),

		.memToReg_out(MEM_TO_REG3),
		.regWrite_out(REG_WRITE3),
		.memRead_out(MEM_READ3),
		.notLockMem_out(NOT_LOCK_MEM3),
		.memWrite_out(MEM_WRITE3),
		.branch_out(BRANCH3),
		.branchAddr_out(BRANCH_ADDR2),
		.aluResult_out(ALU_RESULT2),
		.readData2_out(READ_DATA2_3),
		.rt_or_rd_out(WRITE_REG2),
		.zero_out(ZERO2)
	);
	
	MEM_WB MEMWB (
		.stall(STALL),
		.clk(mainClock),
		.memToReg(MEM_TO_REG3),
		.regWrite(REG_WRITE3),
		.memReadData(MEM_READ_DATA),
		.aluResult(ALU_RESULT2),
		.rt_or_rd(WRITE_REG2),

		.memToReg_out(MEM_TO_REG4),
		.regWrite_out(REG_WRITE4),
		.memReadData_out(MEM_READ_DATA2),
		.aluResult_out(ALU_RESULT3),
		.rt_or_rd_out(WRITE_REG3)
	);
	
	HazardDetector harzard_detector (
		.stall(STALL),
		.memRead_idex(MEM_READ2),
		.rt_idex(RT),
		.rs_ifid(INST2[25:21]),
		.rt_ifid(INST2[20:16]),
		.pc_write(PC_WRITE),
		.ifid_write(IFID_WRITE),
		.noop(NOOP)
	);
	
	InstOrZero instOrZero (
		.RAW_INST(RAW_INST),
		.jump(IF_FLUSH),
		.INST(INST)
	);
	
	BranchHazardUnit branchUnit (
		.stall(STALL),
		.branch(BRANCH),
		.readData1(READ_DATA1),
		.readData2(READ_DATA2),
		.signExt(SIGN_EXT_DATA),
		.pc_plus_4(PC_PLUS_4_2),
		.branch_and_zero(BRANCH_AND_ZERO),
		.pc_p_4_p_br_addr(PC_P4_P_BRANCH_ADDR),
		.ifid_write(FLUSH_BY_BRANCH)
	);
	
	assign IF_FLUSH = FLUSH_BY_BRANCH || JUMP;
endmodule

module mux_3 (
		input stall,
		input [31:0] input0,
		input [31:0] input1,
		input [31:0] input2,
		input [1:0] forward,

		output reg [31:0] out
	);

	always @ (*)
	begin
		if (stall == 0)
		begin
			case (forward)
				2'b00 : out = input0;
				2'b01 : out = input1;
				2'b10 : out = input2;
			endcase
		end
	end
endmodule

module ForwardUnit (
		input stall,
		input [4:0] rt_idex,
		input [4:0] rs_idex,
		input [4:0] rd_exmem,
		input [4:0] rd_memwb,
		input reg_write_exmem,
		input reg_write_memwb,
		
		output reg [1:0] forwardA,
		output reg [1:0] forwardB
	);

	always @(*)
	begin
		if (stall == 0)
		begin
			forwardA = 2'b00;
			forwardB = 2'b00;
			if (reg_write_exmem && rd_exmem != 0 && rd_exmem == rs_idex)
				forwardA = 2'b10;
			if (reg_write_exmem && rd_exmem != 0 && rd_exmem == rt_idex)
				forwardB = 2'b10;
			if (reg_write_memwb && rd_memwb != 0 && rd_memwb == rs_idex && rd_exmem != rs_idex)
				forwardA = 2'b01;
			if (reg_write_memwb && rd_memwb != 0 && rd_memwb == rt_idex && rd_exmem != rt_idex)
				forwardB = 2'b01;
		end
	end
endmodule

module IF_ID (
		input stall,
		input clk,
		input ifid_write,
		input [31:0] pc,
		input [31:0] inst,

		output reg [31:0] pc_out,
		output reg [31:0] inst_out
	);

	initial
	begin
		pc_out = 32'b00000000000000000000000000000000;
		inst_out = 32'b00000000000000000000000000000000;
	end

	always @ (posedge clk)
	begin
		if (stall == 0)
		begin
			if (ifid_write)
			begin
				pc_out = pc;
				inst_out = inst;
			end
		end
	end
endmodule

module ID_EX (
		input stall,
		input clk,
		input regDst,
		input aluSrc,
		input memToReg,
		input regWrite,
		input memRead,
		input notLockMem,
		input memWrite,
		input branch,
		input [1:0] aluOp,
		input [31:0] pc,
		input [31:0] immediate,
		input [31:0] readData1,
		input [31:0] readData2,
		input [4:0] rt,
		input [4:0] rd,
		input [4:0] rs,

		output reg regDst_out,
		output reg aluSrc_out,
		output reg memToReg_out,
		output reg regWrite_out,
		output reg memRead_out,
		output reg notLockMem_out,
		output reg memWrite_out,
		output reg branch_out,
		output reg [1:0] aluOp_out,
		output reg [31:0] pc_out,
		output reg [31:0] immediate_out,
		output reg [31:0] readData1_out,
		output reg [31:0] readData2_out,
		output reg [4:0] rt_out,
		output reg [4:0] rd_out,
		output reg [4:0] rs_out
	);

	always @ (posedge clk)
	begin
		if (stall == 0)
		begin
			regDst_out = regDst;
			aluSrc_out = aluSrc;
			memToReg_out = memToReg;
			regWrite_out = regWrite;
			memRead_out = memRead;
			notLockMem_out = notLockMem;
			memWrite_out = memWrite;
			branch_out = branch;
			aluOp_out = aluOp;
			pc_out = pc;
			immediate_out = immediate;
			readData1_out = readData1;
			readData2_out = readData2;
			rt_out = rt;
			rd_out = rd;
			rs_out = rs;
		end
	end
endmodule

module EX_MEM (
		input stall,
		input clk,
		input memToReg,
		input regWrite,
		input memRead,
		input notLockMem,
		input memWrite,
		input branch,
		input [31:0] branchAddr,
		input [31:0] aluResult,
		input [31:0] readData2,
		input [4:0] rt_or_rd,
		input zero,

		output reg memToReg_out,
		output reg regWrite_out,
		output reg memRead_out,
		output reg notLockMem_out,
		output reg memWrite_out,
		output reg branch_out,
		output reg [31:0] branchAddr_out,
		output reg [31:0] aluResult_out,
		output reg [31:0] readData2_out,
		output reg [4:0] rt_or_rd_out,
		output reg zero_out
	);

	initial
	begin
		memToReg_out = 0;
		regWrite_out = 0;
		memRead_out = 0;
		notLockMem_out = 0;
		memWrite_out = 0;
		branch_out = 0;
		branchAddr_out = 32'b00000000000000000000000000000000;
		aluResult_out = 32'b00000000000000000000000000000000;
		readData2_out = 32'b00000000000000000000000000000000;
		rt_or_rd_out = 5'b00000;
		zero_out = 0;
	end
	 
	always @ (posedge clk)
	begin
		if (stall == 0)
		begin
			memToReg_out = memToReg;
			regWrite_out = regWrite;
			memRead_out = memRead;
			notLockMem_out = notLockMem;
			memWrite_out = memWrite;
			branch_out = branch;
			branchAddr_out = branchAddr;
			aluResult_out = aluResult;
			readData2_out = readData2;
			rt_or_rd_out = rt_or_rd;
			zero_out = zero;
		end
	end
endmodule

module MEM_WB (
		input stall,
		input clk,
		input memToReg,
		input regWrite,
		input [31:0] memReadData,
		input [31:0] aluResult,
		input [4:0] rt_or_rd,

		output reg memToReg_out,
		output reg regWrite_out, 
		output reg [31:0] memReadData_out,
		output reg [31:0] aluResult_out,
		output reg [4:0] rt_or_rd_out
	);

	always @ (posedge clk)
	begin
		if (stall == 0)
		begin
			memToReg_out = memToReg;
			regWrite_out = regWrite;
			memReadData_out = memReadData;
			aluResult_out = aluResult;
			rt_or_rd_out = rt_or_rd;
		end
	end
endmodule

module HazardDetector (
		input stall,
		input memRead_idex,
	 	input [4:0] rt_idex,
	 	input [4:0] rs_ifid,
	 	input [4:0] rt_ifid,

	 	output reg pc_write,
	 	output reg ifid_write,
	 	output reg noop
	);

	always @ (*)
	begin
		if (stall == 0)
		begin
			pc_write = 1;
			ifid_write = 1;
			noop = 0;
			if (memRead_idex && (rt_idex == rs_ifid || rt_idex == rt_ifid))
			begin
				pc_write = 0;
				ifid_write = 0;
				noop = 1;
			end
		end
	end
endmodule

module InstOrZero(
		input [31:0] RAW_INST,
		input jump,

		output [31:0] INST
	);

	assign INST = jump ? 32'b00000000000000000000000000000000 : RAW_INST;
endmodule

module BranchHazardUnit(
		input stall,
		input branch,
		input [31:0] readData1,
		input [31:0] readData2,
		input [31:0] signExt,
		input [31:0] pc_plus_4,

		output reg branch_and_zero,
		output reg [31:0] pc_p_4_p_br_addr,
		output reg ifid_write
	);

	always @ (*)
	begin
		if (stall == 0)
		begin
			pc_p_4_p_br_addr = 0;
			ifid_write = 0;
			branch_and_zero = 0;
			if (branch && readData1 == readData2)
			begin
				branch_and_zero = 1;
				pc_p_4_p_br_addr = pc_plus_4 + (signExt << 2);
				ifid_write = 1;
			end
		end
	end
endmodule
