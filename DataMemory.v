`timescale 1ns / 10ps

module DataMemory (
		input stall,
		input notLock,
		input clockIn,
		input memWrite,
		input memRead,
		input [31:0] address,
		input [31:0] writeData,

		output reg success,
		output wire [31:0] readData
	);

	parameter SIZE = 255;

	reg [31:0] memoryFile[0:SIZE];
	reg [31:0] cache[0:SIZE];
	reg [31:0] tag[0:SIZE];
	reg [31:0] temp;

	integer i;

	assign readData = temp;

	initial
	begin
		for (i = 0; i < SIZE; i = i + 1)
		begin
			memoryFile[i] = 32'b00000000000000000000000000000000;
			tag[i] = 32'b11111111111111111111111111111111;
		end

		success = 1;
		temp = 32'b00000000000000000000000000000000;
		//read data
    	$readmemb("dataMem.txt", memoryFile);
		//read data
	end

	always @ (negedge clockIn)
	begin
		if (stall == 0)
		begin
			temp = 32'b00000000000000000000000000000000;
			if (notLock)
			begin
				success = 0;
				if (tag[address[7:4]] != address[31:4]) //NOT HIT!
				begin
					#400
					for (i = 0; i < 16; i = i + 1)
					begin
						if (tag[address[7:4]] != 32'b11111111111111111111111111111111) memoryFile[(tag[address[7:4]] << 4) + i] = cache[(address[7:4] << 4) + i];
						cache[(address[7:4] << 4) + i] = memoryFile[(address[31:4] << 4) + i];
					end

					tag[address[7:4]] = address[31:4];
				end
				
				if (memRead)
				begin
					success = 1;
					temp = cache[address[7:0]];
				end
				else
					temp = 32'b00000000000000000000000000000000;

				if (memWrite)
				begin
					success = 1;
					cache[address[7:0]]= writeData;
				end
			end
		end
	end
endmodule
