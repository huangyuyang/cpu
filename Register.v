`timescale 1ns / 10ps

module Register (
		input clockIn,
		input reset,
		input regWrite,
		input [4:0] readReg1,
		input [4:0] readReg2,
		input [4:0] writeReg,
		input [31:0] writeData,

		output wire [31:0] readData1,
		output wire [31:0] readData2
	);

	reg [31:0] registerFile[0:31];

	integer i;

	initial
	begin
		for (i = 0; i < 32; i = i + 1)
			registerFile[i] = 32'b00000000000000000000000000000000;
	end

	assign readData1 = registerFile[readReg1];
	assign readData2 = registerFile[readReg2];

	always @ (negedge clockIn)
	begin
		if (regWrite)
			registerFile[writeReg] = writeData;
		if (reset)
			for (i = 0; i < 32; i = i + 1)
				registerFile[i] = 32'b00000000000000000000000000000000;
	end
endmodule
