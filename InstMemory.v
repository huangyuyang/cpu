`timescale 1ns / 10ps

module InstMemory (
		input stall,
		input [31:0] address,

		output reg [31:0] readData
	);

	parameter SIZE = 100;

	reg [31:0] memoryFile[0:SIZE];
	integer i;

	initial
	begin
		for (i = 0; i < SIZE; i = i + 1)
			memoryFile[i] = 32'b00000000000000000000000000000000;
		//read data
		$readmemb("instMem1.txt", memoryFile);
		//read data
	end

	always @ (address)
	begin
		if (stall == 0)
		begin
			readData = memoryFile[address[6:0] >> 2];
		end
	end
endmodule
