`timescale 1ns / 10ps

module Signext (
		input [15:0] inst,
		
		output [31:0] data
	);

	assign data = (inst[15]) ? {16'b1111111111111111, inst} : {16'b0000000000000000, inst};
endmodule
