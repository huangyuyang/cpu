`timescale 1ns / 10ps

module PC (
		input stall,
		input clockIn,
		input reset,
		input [31:0] source,
		input pcWrite,

		output reg [31:0] pc
	);

	initial
	begin
		pc = 32'b00000000000000000000000000000000;
	end

	always @ (posedge clockIn)
	begin
		if (stall == 0)
		begin
			if (pcWrite)
			begin
				pc = source;
			end

			if (reset)
				pc = 32'b00000000000000000000000000000000;
		end
	end
endmodule
