`timescale 1ns/10ps
module pipeline ();
  reg clock;
  wire [31:0] pcaddr;
  initial
  begin
    clock=0;
  end
  always #1 clock=~clock;
  Top top(
    .mainClock(clock),
    .reset(1'b0),
    .PC_ADDRESS(pcaddr)
  	 );

endmodule
